import "./topicpage.css";
import {
  getAllTopicsByUserid,
  getAlltopicsCreatedByUserId,
} from "../../util/api";
import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import TopicPageFeed from "./TopicPageFeed";
import { Row } from "react-bootstrap";
import withAuth from "../Hoc/Guarded";

const TopicPage = () => {
  const [Topics, setTopics] = useState([]);
  const [myTopics, setmyTopics] = useState([]);
  const id = localStorage.getItem("id");

  useEffect(() => {
    async function fetchData() {
      const allCreatedTopicsByUserId = await getAlltopicsCreatedByUserId(id);
      setTopics(allCreatedTopicsByUserId);
      const relevantTopics = await getAllTopicsByUserid(id);
      setmyTopics(relevantTopics);
    }
    fetchData();
  }, []);

  const history = useHistory();
  const viewTopic = (topicId) => {
    history.push(`/topic/${topicId}`);
  };

  const myTopicRender = Topics.map((topic, index) => {
    return (
      <TopicPageFeed key={index} TopicValue={topic} Topicselected={viewTopic} />
    );
  });

  const topicRenderer = myTopics.map((mytopic, index) => {
    return (
      <TopicPageFeed
        key={index}
        TopicValue={mytopic}
        Topicselected={viewTopic}
      />
    );
  });

  return (
    <div className="topic-page">
      <div className="topic-page-container">
        <h3 className="topic-title">Owner of topics</h3>
        <Row>{myTopicRender}</Row>
      </div>
      <div className="topic-page-container">
        <h3 className="topic-title">All topics</h3>
        <Row>{topicRenderer}</Row>
      </div>
    </div>
  );
};

export default withAuth(TopicPage);

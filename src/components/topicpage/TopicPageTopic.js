import "./topicpage.css";
import { Link } from "react-router-dom";
import { useParams, useHistory } from "react-router";
import { useState, useEffect } from "react";
import {
  getAllTopicsById,
  getAllPostInTopicByTopicId,
  GetAllEventsByTopicId,
  createPost,
  JoinTopic,
  getUserByUserId,
  getAllMembersInTopic,
} from "../../util/api";
import TopicPagePosts from "../eventpage/EventPagePosts";
import TopicPageButton from "../grouppage/GroupPageButton";
import { Breadcrumb, Row, Col, ListGroup } from "react-bootstrap";
import TopicPageForm from "../grouppage/GroupPageForm";
import JoinButton from "./TopicPageJoinButton";
import withAuth from "../Hoc/Guarded";

const TopicPageTopic = () => {
  const [Topic, setTopic] = useState([]);
  const userId = parseInt(localStorage.getItem("id"));
  const { id } = useParams();
  const [show, setShow] = useState(false);
  const history = useHistory();
  const [TopicPost, setTopicPost] = useState([]);
  const [Events, setEvents] = useState([]);
  const [Users, setUsers] = useState([]);

  useEffect(() => {
    async function fetchData() {
      // fetch topic
      const fetchedDataGroup = await getAllTopicsById(id);
      setTopic(fetchedDataGroup);
      // fetch posts in topic
      const fetchedDataPosts = await getAllPostInTopicByTopicId(id);
      setTopicPost(fetchedDataPosts);
      // fetch events that belongs to a topic
      const events = await GetAllEventsByTopicId(id);
      setEvents(events);
      const users = await getAllMembersInTopic(id);
      setUsers(users);
    }
    fetchData();
  }, []);

  const handleClose = () => {
    setShow(false);
  };

  const handleShow = () => {
    setShow(true);
  };

  const createTopicPost = async (title, description) => {
    const post = await createPost(title, 2, userId, description, id);
    post.firstName = localStorage.getItem("firstName");
    post.lastName = localStorage.getItem("lastName");
    setTopicPost([...TopicPost, post]);
    setShow(false);
  };

  const viewPost = (postValue) => {
    history.push(`/post/${postValue.postId}`);
  };

  const PostRenderer = TopicPost.map((TopicPost, index) => {
    return (
      <TopicPagePosts
        postValue={TopicPost}
        key={index}
        postSelected={viewPost}
      />
    );
  });

  const EventRender = Events.map((events, index) => {
    return (
      <ListGroup.Item className="events" key={index}>
        {events.name}
      </ListGroup.Item>
    );
  });

  const handleOnclickJoinButton = async () => {
    const userId = localStorage.getItem("id");
    const response = await JoinTopic(id, userId);
    if (response === "user added") {
      const user = await getUserByUserId(userId);
      setUsers([...Users, user]);
    }
  };

  const exist = () => {
    let check = false;
    Users.forEach((user) => {
      if (user.userId === userId) {
        check = true;
      }
    });
    return check;
  };

  const renderJoinButton = () => {
    if (exist()) {
      return;
    }
    return <JoinButton onPress={handleOnclickJoinButton} />;
  };

  const userRenderer = Users.map((user, index) => {
    return (
      <ListGroup.Item className="events" key={index}>
        {user.firstName} {user.lastName}
      </ListGroup.Item>
    );
  });

  return (
    <div>
      <Breadcrumb>
        <Breadcrumb.Item linkAs={Link} linkProps={{ to: "/" }}>
          Alumni
        </Breadcrumb.Item>
        <Breadcrumb.Item linkAs={Link} linkProps={{ to: "/topics" }}>
          Topics
        </Breadcrumb.Item>
        <Breadcrumb.Item active>{id}</Breadcrumb.Item>
      </Breadcrumb>
      <div className="event-eventpage-feed">
        <div className="event-info-container">
          <h5>{Topic.name}</h5>
          <p className="description">{Topic.description}</p>
        </div>
        <div className="create-button-container">
          {renderJoinButton()}
          <TopicPageButton onPress={handleShow} />
        </div>
      </div>
      <Row className="members-group-container my-4">
        <Col md="5">
          <Col md="6" className="my-3">
            <ListGroup>
              <h5>Members</h5>
              {userRenderer}
            </ListGroup>
          </Col>
          <Col md="6" className="my-3">
            <ListGroup>
              <h5>Events</h5>
              {EventRender}
            </ListGroup>
          </Col>
        </Col>
        <Col md="7">
          <h5 className="mb-3">POSTS</h5>
          <div>{PostRenderer}</div>
        </Col>
      </Row>
      <TopicPageForm
        show={show}
        createGroupPost={createTopicPost}
        hideModal={handleClose}
      />
    </div>
  );
};

export default withAuth(TopicPageTopic);

import "./startpage.css";

const StartPageInputSearch = (props) => {
  const handleInputChange = (event) => {
    props.handleInputChange(event.target.value);
  };

  return (
    <div>
      <input
        placeholder="Search for topics, groups and events"
        className="startpage-input-search"
        onChange={handleInputChange}
      />
    </div>
  );
};

export default StartPageInputSearch;

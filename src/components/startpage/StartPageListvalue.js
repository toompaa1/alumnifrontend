import { useState, useEffect } from "react";
import { ListGroup } from "react-bootstrap";
import { LockFill } from "react-bootstrap-icons";

const StartPageListValue = ({ valueObject, handleLiClicked }) => {
  const [type, setType] = useState("");

  useEffect(() => {
    const checkType = () => {
      if (valueObject.hasOwnProperty("groupId")) {
        setType("Group");
      } else if (valueObject.hasOwnProperty("topicId")) {
        setType("Topic");
      } else if (valueObject.hasOwnProperty("eventId")) {
        setType("Event");
      }
    };
    checkType();
  }, [valueObject]);
  const handleClick = () => {
    handleLiClicked(valueObject);
  };

  return (
    <ListGroup.Item onClick={handleClick} className="search-list-item">
      {type}: {valueObject.name}{" "}
      {valueObject.isPrivate === true ? <LockFill /> : ""}
    </ListGroup.Item>
  );
};

export default StartPageListValue;

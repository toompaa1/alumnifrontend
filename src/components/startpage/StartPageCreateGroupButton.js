import "./startpage.css";
import { Button } from "react-bootstrap";

const StartPageCreateGroupButton = ({ onPress }) => {
  return (
    <div>
      <Button
        variant="outline-light"
        className="create-startpage-button"
        onClick={onPress}
      >
        Create Group
      </Button>
    </div>
  );
};

export default StartPageCreateGroupButton;

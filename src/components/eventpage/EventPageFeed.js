import './eventpage.css';
import { Button, Card, Col } from 'react-bootstrap';

const EventPageFeed = ({ eventValue, eventSelected }) => {
  const eventClicked = () => {
    eventSelected(eventValue);
  };
  return (
    <Col md="6" className="my-2">
      <Card className="text-center">
        <Card.Header>Event</Card.Header>
        <Card.Body>
          <Card.Title>{eventValue.name}</Card.Title>
          <Card.Text>{eventValue.description}</Card.Text>
          <Button variant="primary" onClick={eventClicked}>
            Go to event
          </Button>
        </Card.Body>
        <Card.Footer className="text-muted">
          Event created: {eventValue.startDate} - Take place: {eventValue.endDate}
        </Card.Footer>
      </Card>
    </Col>
  );
};

export default EventPageFeed;

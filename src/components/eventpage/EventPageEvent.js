import { useState, useEffect } from 'react';
import CreatePost from './eventPageButton';
import EventPageForm from './EventPageForm';
import {
  createPost,
  getAllGroupsInEventByEventId,
  getAllPostsInEvent,
  getAllTopicsInEventByEventId,
  getAllUsersInEventByEventId,
  getEventById,
  JoinEvent,
  getUserByUserId,
  InviteEventMember
} from '../../util/api';
import { useHistory, useParams } from 'react-router';
import EventPagePosts from './EventPagePosts';
import { ListGroup, Row, Col, Breadcrumb } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import JoinButton from './EventPageJoinButton';
import ListItem from '../globalchildcomponent/listItem';
import InviteButton from './EventPageInviteButton';
import EventPageInviteForm from './EventPageInviteForm';
import withAuth from '../Hoc/Guarded';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const EventPageEvent = () => {
  const [show, setShow] = useState(false);
  const [showInviteModal, setShowInviteModal] = useState(false);
  const [eventPost, setEventPost] = useState([]);
  const [event, setEvent] = useState([]);
  const [users, setUsers] = useState([]);
  const [groups, setGroups] = useState([]);
  const [topics, setTopics] = useState([]);
  const userId = parseInt(localStorage.getItem('id'));
  const { id } = useParams();
  const history = useHistory();

  useEffect(() => {
    async function fetchData() {
      const posts = await getAllPostsInEvent(id);
      const event = await getEventById(id);
      const users = await getAllUsersInEventByEventId(id);
      const groups = await getAllGroupsInEventByEventId(id);
      const topics = await getAllTopicsInEventByEventId(id);
      setGroups(groups);
      setEventPost(posts);
      setEvent(event);
      setUsers(users);
      setTopics(topics);
    }
    fetchData();
  }, []);

  const handleClose = () => {
    setShow(false);
  };

  const handleShow = () => {
    setShow(true);
  };

  const handleCloseInvite = () => {
    setShowInviteModal(false);
  };

  const handleShowInvite = () => {
    setShowInviteModal(true);
  };

  const createEventPost = async (title, description) => {
    const post = await createPost(title, 0, userId, description, id);
    post.firstName = localStorage.getItem("firstName");
    post.lastName = localStorage.getItem("lastName");
    setEventPost([...eventPost, post]);
    setShow(false);
  };

  const handleOnclickJoinButton = async () => {
    const response = await JoinEvent(id, userId);
    if (response === "user added") {
      const user = await getUserByUserId(userId);
      setUsers([...users, user]);
    }
  };

  const exist = () => {
    let check = false;
    users.forEach(user => {
      if (user.userId === userId) {
        check = true;
      }
    });
    return check;
  }

  const renderJoinButton = () => {
    if (exist()) {
      return;
    }
    return (
      <JoinButton onPress={handleOnclickJoinButton} />
    )
  }

  const viewPost = (postValue) => {
    history.push(`/post/${postValue.postId}`);
  };

  const postRenderer = eventPost.map((posts, index) => {
    return (
      <EventPagePosts postValue={posts} key={index} postSelected={viewPost} />
    );
  });

  const handleClickUser = (user) => {
    history.push(`/user/${user.userId}`);
  }

  const userRenderer = users.map((user, index) => {
    return (
      <ListItem object={user} key={index} selectedItem={handleClickUser}></ListItem>
    );
  });

  const handleClickGroup = (group) => {
    if (!group.isPrivate) {
      history.push(`/group/${group.groupId}`);
    }
  }

  const groupsRenderer = groups.map((group, index) => {
    return (
      <ListItem object={group} key={index} selectedItem={handleClickGroup}></ListItem>
    )
  });

  const handleClickTopic = (topic) => {
    if (!topic.isPrivate) {
      history.push(`/topic/${topic.topicId}`);
    }
  }
  const topicsRenderer = topics.map((topic, index) => {
    return (
      <ListItem object={topic} key={index} selectedItem={handleClickTopic}></ListItem>
    );
  });

  const checkIfOwner = () => {
    let check = false;
    if (event.userId === userId) {
      check = true;
    }
    return check;
  }

  const inviteMember = async (userTargetId) => {
    const response = await InviteEventMember(userId, userTargetId, id)
    if (response === "Can not invite yourself") {
      toast.error(response)
      setShowInviteModal(false);
    }
    else if (response === "This invite is pending or the user has already joined the event.") {
      toast.error(response)
      setShowInviteModal(false);
    } else {
      toast.success("Invited")
      setShowInviteModal(false);
    }
  }

  const renderInviteButton = () => {
    if (!checkIfOwner()) {
      return;
    } else {
      return (
        <InviteButton onPress={handleShowInvite} />
      )
    }
  }

  return (
    <div>
      <Breadcrumb>
        <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/' }}>
          Alumni
        </Breadcrumb.Item>
        <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/events' }}>
          Events
        </Breadcrumb.Item>
        <Breadcrumb.Item active>{id}</Breadcrumb.Item>
      </Breadcrumb>
      <div className="event-eventpage-feed">
        <div className="event-info-container">
          <h5>{event.name}</h5>
          <p className="description">{event.description}</p>
          <p className="date">
            {event.startDate} - {event.endDate}
          </p>
        </div>
        <div className="create-button-container">
          {renderJoinButton()} {renderInviteButton()}
          <CreatePost onPress={handleShow} />
        </div>
      </div>
      <Row className="members-group-container my-4">
        <Col md="5">
          <Col md="6" className="my-3">
            <ListGroup>
              <h5>Members</h5>
              {userRenderer}
            </ListGroup>
          </Col>
          <Col md="6" className="my-3">
            <ListGroup>
              <h5>Groups</h5>
              {groupsRenderer}
            </ListGroup>
          </Col>
          <Col md="6" className="my-3">
            <ListGroup>
              <h5>Topics</h5>
              {topicsRenderer}
            </ListGroup>
          </Col>
        </Col>
        <Col md="7">
          <h5 className="mb-3">POSTS</h5>
          <div>{postRenderer}</div>
        </Col>
      </Row>
      <EventPageForm
        show={show}
        createEventPost={createEventPost}
        hideModal={handleClose}
      />
      <EventPageInviteForm showInviteForm={showInviteModal} inviteMember={inviteMember} hideModal={handleCloseInvite} />
      <ToastContainer />
    </div>
  );
};

export default withAuth(EventPageEvent);

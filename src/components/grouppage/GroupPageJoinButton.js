import "./grouppage.css";
import { Button } from "react-bootstrap";

const JoinButton = ({ onPress }) => {
  return (
    <Button className="mx-3" variant="outline-light" onClick={onPress}>
      Join Group
    </Button>
  );
};

export default JoinButton;

import "./grouppage.css";
import { Button, Card, Col } from "react-bootstrap";

const GroupPageFeed = ({ groupValue, groupselected }) => {
  const groupClicked = () => {
    groupselected(groupValue.groupId);
  };

  return (
    <Col md="6" className="my-2">
      <Card className="text-center">
        <Card.Header>Group</Card.Header>
        <Card.Body>
          <Card.Title>{groupValue.name}</Card.Title>
          <Card.Text>{groupValue.description}</Card.Text>
          <Button variant="primary" onClick={groupClicked}>
            Go to group
          </Button>
        </Card.Body>
      </Card>
    </Col>
  );
};

export default GroupPageFeed;

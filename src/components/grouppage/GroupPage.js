import "./grouppage.css";
import GroupPageFeed from "./GroupPageFeed";
import { getAllGroupByUserId, GetGroupsByCreatedUserId } from "../../util/api";
import { useState, useEffect } from "react";
import { Row } from "react-bootstrap";
import withAuth from "../Hoc/Guarded";

import { useHistory } from "react-router-dom";

const GroupPage = () => {
  const userId = localStorage.getItem("id");
  const [Groups, setGroups] = useState([]);
  const [createdGroups, setCreatedGroups] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const fetchedData = await getAllGroupByUserId(userId);
      const createdGroups = await GetGroupsByCreatedUserId(userId);
      setGroups(fetchedData);
      setCreatedGroups(createdGroups);
    }
    fetchData();
  }, []);

  const history = useHistory();
  const viewGroup = (groupId) => {
    history.push(`/group/${groupId}`);
  };

  const groupRenderer = Groups.map((group, index) => {
    return (
      <GroupPageFeed key={index} groupValue={group} groupselected={viewGroup} />
    );
  });

  const createdGroupRenderer = createdGroups.map((group, index) => {
    return (
      <GroupPageFeed key={index} groupValue={group} groupselected={viewGroup} />
    );
  });

  return (
    <div>
      <div className="group-page-container">
        <h3 className="group-title">Owner of groups</h3>
        <Row>{createdGroupRenderer}</Row>
      </div>
      <div className="group-page-container">
        <h3 className="group-title">Member of groups</h3>
        <Row>{groupRenderer}</Row>
      </div>
    </div>
  );
};

export default withAuth(GroupPage);

import "./App.css";
import StartPage from "./components/startpage/StartPage";
import EventPage from "./components/eventpage/EventPage";
import AlumniNavbar from "./components/navbar/Navbar";
import GroupPage from "./components/grouppage/GroupPage";
import TopicPage from "./components/topicpage/TopicPage";
import ProfilePage from "./components/profilepage/ProfilePage";
import EventPageEvent from "./components/eventpage/EventPageEvent";
import { UserContext } from "./context";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import EventPagePost from "./components/eventpage/EventPagePost";
import { Container } from "react-bootstrap";
import GroupPageGroup from "./components/grouppage/GroupPageGroup";
import TopicPageTopic from "./components/topicpage/TopicPageTopic";
import ProfilePageProfile from "./components/profilepage/ProfilePageProfile";
import { useEffect, useState } from "react";
import { getUserByKeycloakId, postUser } from "./util/api";
import Keycloak from "keycloak-js";

function App() {
  const [load, setLoad] = useState(false);
  const [keycloak, setKeycloak] = useState(null);
  const [authenticated, setAuthenticated] = useState(false);
  const [token, setToken] = useState(null);
  useEffect(() => {
    async function checkUser(keycloak) {
      const keycloakId = keycloak.idTokenParsed.sub;
      const data = await getUserByKeycloakId(keycloakId);
      if (data === "Not Found") {
        const keycloakUser = {
          firstName: keycloak.idTokenParsed.given_name,
          lastName: keycloak.idTokenParsed.family_name,
          keycloakId: keycloakId,
        };
        const user = await postUser(keycloakUser);
        localStorage.setItem("id", user.userId);
        localStorage.setItem("firstName", user.firstName);
        localStorage.setItem("lastName", user.lastName);
        setLoad(true);
      } else {
        localStorage.setItem("firstName", data.firstName);
        localStorage.setItem("lastName", data.lastName);
        localStorage.setItem("id", data.userId);
        setLoad(true);
      }
    }
    const keycloak = Keycloak("/keycloak.json");
    keycloak.init({ onLoad: "login-required" }).then((authenticated) => {
      setKeycloak(keycloak);
      setAuthenticated(authenticated);
      setToken(keycloak.token);
      localStorage.setItem("token", keycloak.token);
      checkUser(keycloak);
    });
  }, []);
  return (
    <Router>
      <UserContext.Provider value={keycloak}>
        <div className="App">
          <header>
            <AlumniNavbar />
          </header>
          <main>
            {load && (
              <Container>
                <Switch>
                  <Route exact path="/" component={StartPage} />
                  <Route path="/events" component={EventPage} />
                  <Route path="/groups" component={GroupPage} />
                  <Route path="/topics" component={TopicPage} />
                  <Route path="/profile" component={ProfilePage} />
                  <Route path="/event/:id" component={EventPageEvent} />
                  <Route path="/post/:id" component={EventPagePost} />
                  <Route path="/group/:id" component={GroupPageGroup} />
                  <Route path="/topic/:id" component={TopicPageTopic} />
                  <Route path="/user/:id" component={ProfilePageProfile} />
                </Switch>
              </Container>
            )}
          </main>
          <footer></footer>
        </div>
      </UserContext.Provider>
    </Router>
  );
}

export default App;

import { createContext } from "react";
/*
  Here we create the UserContext.
*/
export const UserContext = createContext();
